/*The factors come as two type of objects 
 * 1) A = spin-color vector for each time slice = T * 12 component object = A[time][color][spin] = 3-D array
 * 2) B = spin3, spin2, spin1 - color object for each time slice  = T * 4 * 4 * 4 * 3 = T* 192 component object 
 * =  B[time][color][spin1][spin2][spin3]
 * We are creating two template data-types called A [time][color][spin], and B [time][color][spin1][spin2][spin3]*/
namespace factor {
 
    template <class T, size_t time, size_t color, size_t spin>
        using A = std::array<std::array<std::array<T, spin>, color>, time>;

    template<class T, size_t time, size_t color, size_t spin1, size_t spin2, size_t spin3>
        using B = std::array<std::array<std::array<std::array<std::array<T, spin3>, spin2>, spin1>, color>, time>;
}

namespace spin_dil {

    template <class T, int time, int c, int spin>
        using dil = std::array<std::array<std::array<T, spin>, c>, time>;

//typedef std::array< std::array< std::array<std::complex<double>, 16> ;
}
/*
namespace factor {
    
    class A
    {
        public:
            A( int time, int color, int spin)
                : time(time), color(color), spin(spin), data ( new std::complex<double>[time * color * spin] ){}
            
            A( const A& that)
                : time(that.time), color(that.color), spin(that.spin),
                  data( new std::complex<double>[time * color * spin])
            {
            for ( int i= 0, size = time * color * spin; i < size; ++i)
                data[i] = that.data[i];
            }
    
            void operator=( const A& that)
            {
                assert(time == that.time && color == that.color && spin = that.spin);
                for ( int i = 0, size = time * color * spin; i < size; ++i)
                    data[i] = that.data[i];
            }
            int num_time() const { return time; }
            int num_color()const { return color;}
            int num_spin() const { return spin; } 
    
        private:
            
            int                                 time, color, spin;
            std::unique_ptr<std::complex<double>>    data;
    };

}
*/

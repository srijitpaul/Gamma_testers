#include "lhpc-aff++.h"
#include "data_type.h"
/*2PT OET FACTORS
 * All factors are of two types 
 *
 * A^l_m : spin-color 12 component vector
 * B^l_{pqr} : 192 component object
 *
 *2pt oet factors for pi-N are:
 * 1 factor of A type
 * 2 factors of B type
 *
 * In the code we refer them as A, B and B'
 *
 * This function returns flattened versions of A, B and B'.
 *
 * */
namespace oet_factor{
    
    template<typename T>
        void  aff_reader_A( T &datum, char file_name[100], char oet_fact[100], int mom1[3], int mom2[3], int time) {

            std::complex<double> *buffer = new std::complex<double>[time];
            Aff::Reader f(file_name);
            char key[100];
            Aff::Reader::Node *affn = NULL;
            for(int kspin = 0; kspin < 4; ++kspin) {
                for( int c = 0; c < 12; ++c) {

                    std::snprintf(key, 100, "/%s/pi2x%.2dpi2y%.2dpi2z%.2d/sample00/spin%.2d/color_vector%.2d/gf215/PX%.1d_PY%.1d_PZ%.1d",
                            oet_fact, mom2[0], mom2[1], mom2[2], kspin, c, mom1[0], mom1[1], mom1[2]);
                    affn = f.chdir_path(affn, key);
                    f.get_complex( affn, buffer, time);
                    for ( int t = 0; t < time; ++t) {

                        datum[t][c][kspin]= buffer[t];

                    }
                }
            }

            delete [] buffer;    
        };


    template<typename T>
        void  aff_reader_B( T &datum, char file_name[100], char oet_fact[100], int mom[3], int time) {

            std::complex<double> *buffer = new std::complex<double>[time];
            Aff::Reader f(file_name);
            char key[100];
            Aff::Reader::Node *affn = NULL;
            for(int kspin = 0; kspin < 4; ++kspin) {
                for( int c = 0; c < 192; ++c) {

                    std::snprintf(key, 100, "/%s/sample00/spin%.2d/color_vector%.2d/gf115/PX%.1d_PY%.1d_PZ%.1d",
                            oet_fact, kspin, c, mom[0], mom[1], mom[2]);
                    affn = f.chdir_path(affn, key);
                    f.get_complex( affn, buffer, time);
                    for ( int t = 0; t < time; ++t) {

                        datum[t][c][kspin]= buffer[t];

                    }
                }
            }

            delete [] buffer;    
        };
}


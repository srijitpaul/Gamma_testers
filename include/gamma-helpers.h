# include <iostream>
# include <complex>
# include <array>
//# include <omp.h>
typedef std::array<std::complex<double>, 16> Matrix;
typedef std::complex<double> c;
//Deflating Gamma matrix with Row Major index
typedef std::array<const Matrix, 16> Gamma_list;

const Matrix gamma(int i);
template<typename T>
void printMatrix(T &data, const char *name, int m, int n);
const Matrix gamma(int i) {

    const Matrix gam00{  c(1,0), c(0,0), c(0,0), c(0,0),  c(0,0), c(1,0), c(0,0), c(0,0),  c(0,0), c(0,0),  c(1,0), c(0,0),  c(0,0), c(0,0), c(0,0), c(1,0)};
    const Matrix gam01{  c(0,0), c(0,0), c(0,0), c(0,1),  c(0,0), c(0,0), c(0,1), c(0,0),  c(0,0), c(0,-1), c(0,0), c(0,0),  c(0,-1),c(0,0), c(0,0), c(0,0)};
    const Matrix gam02{  c(0,0), c(0,0), c(0,0), c(-1,0), c(0,0), c(0,0), c(1,0), c(0,0),  c(0,0), c(1,0),  c(0,0), c(0,0),  c(-1,0),c(0,0), c(0,0), c(0,0)};
    const Matrix gam03{  c(0,-1),c(0,0), c(0,0), c(0,0),  c(0,0), c(0,1), c(0,0), c(0,0),  c(0,0), c(0,0),  c(0,-1),c(0,0),  c(0,0), c(0,0), c(0,0), c(0,1)};
    const Matrix gam04{  c(0,0), c(0,0), c(0,1), c(0,0),  c(0,0), c(0,0), c(0,0), c(0,-1), c(0,-1),c(0,0),  c(0,0), c(0,0),  c(0,0), c(0,1), c(0,0), c(0,0)};
    const Matrix gam05{  c(0,0), c(-1,0),c(0,0), c(0,0),  c(1,0), c(0,0), c(0,0), c(0,0),  c(0,0), c(0,0),  c(0,0), c(-1,0), c(0,0), c(0,0), c(1,0), c(0,0)};
    const Matrix gam06{  c(0,0), c(0,-1),c(0,0), c(0,0),  c(0,-1),c(0,0), c(0,0), c(0,0),  c(0,0), c(0,0),  c(0,0), c(0,-1), c(0,0), c(0,0), c(0,-1),c(0,0)};
    const Matrix gam07{  c(0,0), c(0,0), c(1,0), c(0,0),  c(0,0), c(0,0), c(0,0), c(1,0),  c(-1,0),c(0,0),  c(0,0), c(0,0),  c(0,0), c(-1,0),c(0,0), c(0,0)};
    const Matrix gam08{  c(0,0), c(0,0), c(1,0), c(0,0),  c(0,0), c(0,0), c(0,0), c(1,0),  c(1,0), c(0,0),  c(0,0), c(0,0),  c(0,0), c(1,0), c(0,0), c(0,0)};
    const Matrix gam09{  c(0,0), c(0,1), c(0,0), c(0,0),  c(0,1), c(0,0), c(0,0), c(0,0),  c(0,0), c(0,0),  c(0,0), c(0,-1), c(0,0), c(0,0), c(0,-1),c(0,0)};
    const Matrix gam10{  c(0,0), c(-1,0),c(0,0), c(0,0),  c(1,0), c(0,0), c(0,0), c(0,0),  c(0,0), c(0,0),  c(0,0), c(1,0),  c(0,0), c(0,0), c(-1,0),c(0,0)};
    const Matrix gam11{  c(0,0), c(0,0), c(0,-1),c(0,0),  c(0,0), c(0,0), c(0,0), c(0,1),  c(0,-1),c(0,0),  c(0,0), c(0,0),  c(0,0), c(0,1), c(0,0), c(0,0)};
    const Matrix gam12{  c(0,1), c(0,0), c(0,0), c(0,0),  c(0,0), c(0,-1),c(0,0), c(0,0),  c(0,0), c(0,0),  c(0,-1),c(0,0),  c(0,0), c(0,0), c(0,0), c(0,1)};
    const Matrix gam13{  c(0,0), c(0,0), c(0,0), c(-1,0), c(0,0), c(0,0), c(1,0), c(0,0),  c(0,0), c(-1,0), c(0,0), c(0,0),  c(1,0), c(0,0), c(0,0), c(0,0)};
    const Matrix gam14{  c(0,0), c(0,0), c(0,0), c(0,-1), c(0,0), c(0,0), c(0,-1),c(0,0),  c(0,0), c(0,-1), c(0,0), c(0,0),  c(0,-1),c(0,0), c(0,0), c(0,0)};
    const Matrix gam15{  c(1,0), c(0,0), c(0,0), c(0,0),  c(0,0), c(1,0), c(0,0), c(0,0),  c(0,0), c(0,0),  c(-1,0),c(0,0),  c(0,0), c(0,0), c(0,0), c(-1,0)};

    Gamma_list g = { gam00, gam01, gam02, gam03, gam04, gam05, gam06, gam07, gam08, gam09, gam10, gam11, gam12, gam13, gam14, gam15 };

    return g[i];
}

template<typename T>
void printMatrix(T &data, const char *name, int m, int n){
    std::cout << name << "\n";
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            std::cout << data[n*i+j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

template<typename T>
void DGEMM_simd(const int m, const int n, const int k, 
        T &A,
        T &B,  
        T &C) {
    // TODO: Include OpenMP directive

//    int nthreads = omp_get_num_threads();
//    printf("Number of threads = %d\n", nthreads);

    std::complex<double> dum; 
//    if (k != lda || n != ldb || n != ldc)
//        throw std::invalid_argument("Invalid argument!"); 

//#pragma omp parallel for
    for (int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            C[i * n + j] *= 1;
        }

 
        for(int l = 0; l < k; l++) {
            dum = A[i * k + l] ;
//#pragma omp  simd
            for (int j = 0; j < n; j++) {

                C[i * n + j] += dum * B[l * n + j]; 
            }

        }
    }
}


template<typename T>
void DADD(const int m, const int n, 
        T &A,
        T &B,  
        T &C) {
 

	for(int i = 0; i < m; i++) {
     
		for (int j = 0; j < n; j++) {

                C[i * n + j]  =   A[i * n + j] +  B[i * n + j]; 
        }
	}
   
}

template<typename T>
void DSCLMULT(const int m, const int n, 
        double alpha,
		T &A,  
        T &C ){
 

	for(int i = 0; i < m; i++) {
     
		for (int j = 0; j < n; j++) {

                C[i * n + j]  =   A[i * n + j] * alpha; 
        }
	}
   
}


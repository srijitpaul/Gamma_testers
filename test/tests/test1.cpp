//
// Created by spaul on 12.12.17..
//
//
#include "gtest/gtest.h"
#include "gamma-helpers.h"
//#include <mkl.h>
//#include <mkl_cblas.h>
//#define MKL_Complex16 std::complex<double>

TEST(test1, test_eq1) {
   	for(int i = 0; i<16; i++) {
	  	Matrix g1 = gamma(i);
        Matrix g2;
        Matrix Id = gamma(0);
		Matrix neg_Id;
		DSCLMULT<Matrix>( 4, 4, -1, Id, neg_Id);
       
        //std::cout<<"gamma i"<<i<<std::endl;
        //printMatrix<Matrix>(g1, "gamma", 4, 4);
        
        DGEMM_simd<Matrix>( 4, 4, 4, g1, g1, g2);
        
        
        //printMatrix<Matrix>(g2, "product gamma", 4, 4);
        ASSERT_TRUE(g2 == Id|| g2 == neg_Id);
    }        
}

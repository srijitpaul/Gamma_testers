# include<iostream>
# include<fstream>
# include"gamma-helpers.h"
# include"lhpc-aff++.h"
# include"clipp.h"
# include"data_type.h"
# include"aff_reader.h"
int main(int argc, char* argv[]) {


/*Simple Implementation of commandline-parser*/    
  

/*Default parameters*/
    constexpr int L = 8;
    constexpr int T = 16;
    int nsrc_loc = 1;
    std::string config_number = "0001";
    std::ifstream fs;
/********************/
    auto cli = ( 
            clipp::option("-nsrc") & clipp::value("#", nsrc_loc),                // set nsrc_loc's value from arg string  
            clipp::option("-config") & clipp::value("#", config_number),         // set config's value from arg string  
            clipp::option("-f") & clipp::value("file").call([&](std::string f){ fs.open(f); }) // name of the file containing source locations
   );

    parse(argc, argv, cli);

/*Checking all the parsed arguments*/

    /*4-D coordinates of the source location*/
    int **src_loc = new int*[nsrc_loc];
    for(int i = 0; i < nsrc_loc; ++i)
        src_loc[i] = new int[4];
    
    int *temp_var   = new int[4];

    std::string temp;

    std::cout<<"T               = "<<T<<std::endl;
    std::cout<<"L               = "<<L<<std::endl;
    std::cout<<"Reading config  = "<<config_number<<std::endl;

    for(int i = 0; i < nsrc_loc; i++){
        
        fs >> temp >> temp_var[0] >> temp_var[1] >> temp_var[2] >> temp_var[3];
        if(temp.compare(config_number) == 0){
            src_loc[i][0] = temp_var[0]; src_loc[i][1] = temp_var[1]; src_loc[i][2] = temp_var[2]; src_loc[i][3] = temp_var[3];
            std::cout<<"Source location is  = ("<<src_loc[i][0]<<", "<<src_loc[i][1]<<", "<<src_loc[i][2]<<", "<<src_loc[i][3]<<")"<<std::endl;
        }
        else{std::cout<<"There are no source locations for that config number"<<std::endl;}
    }
/**Check complete******************/

    char file_name[100];
    char key[100];
    const char *conf_num = config_number.c_str();

    for( int i = 0; i < nsrc_loc; i++) {


        /*Reading factors for oet diagrams*/
        std::snprintf(file_name, 100, "../../../../pion_nucleon/propagators/tests/%s/contract_oet_mb_factors.%s.x%.2dy%.2dz%.2dt%.2d.00000.aff", 
                conf_num,conf_num, src_loc[i][0], src_loc[i][1], src_loc[i][2], src_loc[i][3]);
        std::cout<<"File name: "<<file_name<<std::endl;
        int pf1[3] = {0};

        spin_dil::dil<std::complex<double>, T, 192, 4 > oetfac_phi_GF1UU;
        std::snprintf(key, 100, "phi--gf1-U-U");
        oet_factor::aff_reader_B<spin_dil::dil<std::complex<double>, T, 192, 4 > >(oetfac_phi_GF1UU, file_name, key, pf1, T);
        std::cout<<"The first term of "<<key<<" = "<<oetfac_phi_GF1UU[0][0][0]<<std::endl;

        spin_dil::dil<std::complex<double>, T, 192, 4 > oetfac_phiGF1U_U;
        std::snprintf(key, 100, "phi-gf1-U--U");
        oet_factor::aff_reader_B<spin_dil::dil<std::complex<double>, T, 192, 4 > >(oetfac_phiGF1U_U, file_name, key, pf1, T);
        std::cout<<"The first term of "<<key<<" = "<<oetfac_phiGF1U_U[0][0][0]<<std::endl;
        
        int pi2[3] = {0};
        spin_dil::dil<std::complex<double>, T, 12, 4 > oetfac_phiGF2U;
        std::snprintf(key, 100, "phi-gf2-U");
        oet_factor::aff_reader_A<spin_dil::dil<std::complex<double>, T, 12, 4 > >(oetfac_phiGF2U, file_name, key, pi2, pf1, T);
        std::cout<<"The first term of "<<key<<" = "<<oetfac_phiGF2U[0][0][0]<<std::endl;


    }
        



/*Delete dynamical arrays*/
 for(int i = 0; i < nsrc_loc; i++)
     delete [] src_loc[i];
 delete [] src_loc;

 delete [] temp_var;
}

